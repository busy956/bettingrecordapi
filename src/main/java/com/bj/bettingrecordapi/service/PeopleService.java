package com.bj.bettingrecordapi.service;

import com.bj.bettingrecordapi.entity.People;
import com.bj.bettingrecordapi.model.people.PeopleCreateRequest;
import com.bj.bettingrecordapi.model.people.PeopleItem;
import com.bj.bettingrecordapi.model.people.PeopleMemoChangeRequest;
import com.bj.bettingrecordapi.model.people.PeopleResponse;
import com.bj.bettingrecordapi.repository.PeopleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PeopleService {
    private final PeopleRepository peopleRepository;

    public People getData(long id) {
        return peopleRepository.findById(id).orElseThrow();
    }

    public void setPeople(PeopleCreateRequest request) {
        People addData = new People();
        addData.setName(request.getName());
        addData.setAge(request.getAge());
        addData.setBirthDay(request.getBirthDay());
        addData.setPhoneNumber(request.getPhoneNumber());
        addData.setEtcMemo(request.getEtcMemo());

        peopleRepository.save(addData);
    }

    public List<PeopleItem> getPeoples() {
        List<People> originList = peopleRepository.findAll();

        List<PeopleItem> result = new LinkedList<>();

        for (People people : originList) {
            PeopleItem addItem = new PeopleItem();
            addItem.setId(people.getId());
            addItem.setName(people.getName());
            addItem.setPhoneNumber(people.getPhoneNumber());

            result.add(addItem);
        }
        return result;
    }

    public PeopleResponse getPeople(long id) {
        People originData = peopleRepository.findById(id).orElseThrow();
        PeopleResponse response = new PeopleResponse();
        response.setId(originData.getId());
        response.setName(originData.getName());
        response.setAge(originData.getAge());
        response.setBirthDay(originData.getBirthDay());
        response.setPhoneNumber(originData.getPhoneNumber());
        response.setEtcMemo(originData.getEtcMemo());

        return response;
    }

    public void putMemo(long id, PeopleMemoChangeRequest request) {
        People originData = peopleRepository.findById(id).orElseThrow();
        originData.setEtcMemo(request.getEtcMemo());

        peopleRepository.save(originData);
    }
}
