package com.bj.bettingrecordapi.service;

import com.bj.bettingrecordapi.entity.CafeRecord;
import com.bj.bettingrecordapi.entity.People;
import com.bj.bettingrecordapi.model.cafeRecord.CafeRecordCreateRequest;
import com.bj.bettingrecordapi.model.cafeRecord.CafeRecordItem;
import com.bj.bettingrecordapi.model.cafeRecord.CafeRecordPriceChangeRequest;
import com.bj.bettingrecordapi.model.cafeRecord.CafeRecordResponse;
import com.bj.bettingrecordapi.repository.CafeRecordRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CafeRecordService {
    private final CafeRecordRepository cafeRecordRepository;

    public void setCafeRecord(People people, CafeRecordCreateRequest request) {
        CafeRecord addData = new CafeRecord();
        addData.setPeople(people);
        addData.setDateCafe(request.getDateCafe());
        addData.setPrice(request.getPrice());

        cafeRecordRepository.save(addData);
    }

    public List<CafeRecordItem> getCafeRecords() {
        List<CafeRecord> originList = cafeRecordRepository.findAll();

        List<CafeRecordItem> result = new LinkedList<>();

        for (CafeRecord cafeRecord : originList) {
            CafeRecordItem addItem = new CafeRecordItem();
            addItem.setPeopleName(cafeRecord.getPeople().getName());
            addItem.setDateCafe(cafeRecord.getDateCafe());
            addItem.setPrice(cafeRecord.getPrice());

            result.add(addItem);
        }
        return result;
    }

    public CafeRecordResponse getCafeRecord(long id) {
        CafeRecord originData = cafeRecordRepository.findById(id).orElseThrow();
        CafeRecordResponse response = new CafeRecordResponse();
        response.setId(originData.getId());
        response.setPeopleName(originData.getPeople().getName());
        response.setPeopleAge(originData.getPeople().getAge());
        response.setPeopleBirthDay(originData.getPeople().getBirthDay());
        response.setPeoplePhoneNumber(originData.getPeople().getPhoneNumber());
        response.setPeopleEtcMemo(originData.getPeople().getEtcMemo());
        response.setDateCafe(originData.getDateCafe());
        response.setPrice(originData.getPrice());

        return response;
    }

    public void putPrice(long id, CafeRecordPriceChangeRequest request) {
        CafeRecord originData = cafeRecordRepository.findById(id).orElseThrow();
        originData.setPrice(request.getPrice());

        cafeRecordRepository.save(originData);
    }

    public void delCafeRecord(long id) {
        cafeRecordRepository.deleteById(id);
    }
}
