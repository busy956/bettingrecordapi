package com.bj.bettingrecordapi.repository;

import com.bj.bettingrecordapi.entity.People;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PeopleRepository extends JpaRepository<People, Long> {
}
