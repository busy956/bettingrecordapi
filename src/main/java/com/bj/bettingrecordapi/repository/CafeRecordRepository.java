package com.bj.bettingrecordapi.repository;

import com.bj.bettingrecordapi.entity.CafeRecord;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CafeRecordRepository extends JpaRepository<CafeRecord, Long> {
}
