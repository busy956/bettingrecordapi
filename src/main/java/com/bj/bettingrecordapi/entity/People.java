package com.bj.bettingrecordapi.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class People {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 20)
    private String name;

    @Column(nullable = false)
    private Short age;

    @Column(nullable = false)
    private LocalDate birthDay;

    @Column(nullable = false, unique = true, length = 13)
    private String phoneNumber;

    @Column(columnDefinition = "TEXT")
    private String etcMemo;
}
