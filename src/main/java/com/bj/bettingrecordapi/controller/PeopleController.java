package com.bj.bettingrecordapi.controller;

import com.bj.bettingrecordapi.model.people.PeopleCreateRequest;
import com.bj.bettingrecordapi.model.people.PeopleItem;
import com.bj.bettingrecordapi.model.people.PeopleMemoChangeRequest;
import com.bj.bettingrecordapi.model.people.PeopleResponse;
import com.bj.bettingrecordapi.service.PeopleService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/people")
public class PeopleController {
    private final PeopleService peopleService;

    @PostMapping("/join")
    public String setPeople(@RequestBody PeopleCreateRequest request) {
        peopleService.setPeople(request);

        return "OK";
    }

    @GetMapping("/all")
    public List<PeopleItem> getPeoples() {
        return peopleService.getPeoples();
    }

    @GetMapping("/detail/{peopleId}")
    public PeopleResponse getPeople(@PathVariable long peopleId) {
        return peopleService.getPeople(peopleId);
    }

    @PutMapping("/memo/people-id/{peopleId}")
    public String putMemo(@PathVariable long peopleId, @RequestBody PeopleMemoChangeRequest request) {
        peopleService.putMemo(peopleId, request);

        return "OK";
    }

}
