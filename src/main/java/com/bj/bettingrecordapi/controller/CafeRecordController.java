package com.bj.bettingrecordapi.controller;

import com.bj.bettingrecordapi.entity.People;
import com.bj.bettingrecordapi.model.cafeRecord.CafeRecordCreateRequest;
import com.bj.bettingrecordapi.model.cafeRecord.CafeRecordItem;
import com.bj.bettingrecordapi.model.cafeRecord.CafeRecordPriceChangeRequest;
import com.bj.bettingrecordapi.model.cafeRecord.CafeRecordResponse;
import com.bj.bettingrecordapi.service.CafeRecordService;
import com.bj.bettingrecordapi.service.PeopleService;
import jakarta.persistence.Id;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/cafe-record")
public class CafeRecordController {
    private final CafeRecordService cafeRecordService;
    private  final PeopleService peopleService;

    @PostMapping("/human/{cafeRecordId}")
    public String setCafeRecord(@PathVariable long cafeRecordId, @RequestBody CafeRecordCreateRequest request) {
        People people = peopleService.getData(cafeRecordId);
        cafeRecordService.setCafeRecord(people, request);

        return "OK";
    }

    @GetMapping("/all")
    public List<CafeRecordItem> getCafeRecords() {
       return cafeRecordService.getCafeRecords();
    }

    @GetMapping("/detail/{cafeRecordId}")
    public CafeRecordResponse getCafeRecord(@PathVariable long cafeRecordId) {
        return cafeRecordService.getCafeRecord(cafeRecordId);
    }

    @PutMapping("/price/cafe-record-id/{cafeRecordId}")
    public String putPrice(@PathVariable long cafeRecordId, @RequestBody CafeRecordPriceChangeRequest request) {
        cafeRecordService.putPrice(cafeRecordId, request);

        return "OK";
    }

    @DeleteMapping("/{cafeRecordId}")
    public void delCafeRecord(@PathVariable long cafeRecordId) {
        cafeRecordService.delCafeRecord(cafeRecordId);
    }
}
