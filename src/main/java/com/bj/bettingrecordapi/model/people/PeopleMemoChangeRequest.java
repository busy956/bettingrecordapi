package com.bj.bettingrecordapi.model.people;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PeopleMemoChangeRequest {
    private String etcMemo;
}
