package com.bj.bettingrecordapi.model.people;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PeopleItem {
    private Long id;
    private String name;
    private String phoneNumber;
}
