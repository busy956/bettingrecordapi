package com.bj.bettingrecordapi.model.people;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class PeopleResponse {
    private Long id;
    private String name;
    private Short age;
    private LocalDate birthDay;
    private String phoneNumber;
    private String etcMemo;
}
