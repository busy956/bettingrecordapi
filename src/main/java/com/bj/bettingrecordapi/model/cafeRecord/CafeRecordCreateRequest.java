package com.bj.bettingrecordapi.model.cafeRecord;

import com.bj.bettingrecordapi.entity.People;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class CafeRecordCreateRequest {
    private LocalDate dateCafe;
    private Double price;
}
