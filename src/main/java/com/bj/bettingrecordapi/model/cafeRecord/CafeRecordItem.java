package com.bj.bettingrecordapi.model.cafeRecord;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class CafeRecordItem {
    private String PeopleName;
    private LocalDate dateCafe;
    private Double price;
}
