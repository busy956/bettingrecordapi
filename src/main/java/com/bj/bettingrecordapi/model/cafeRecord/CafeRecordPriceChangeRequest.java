package com.bj.bettingrecordapi.model.cafeRecord;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CafeRecordPriceChangeRequest {
    private Double price;
}
