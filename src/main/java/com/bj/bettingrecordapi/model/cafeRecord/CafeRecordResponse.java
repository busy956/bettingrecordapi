package com.bj.bettingrecordapi.model.cafeRecord;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class CafeRecordResponse {
    private Long id;
    private String peopleName;
    private Short peopleAge;
    private LocalDate peopleBirthDay;
    private String peoplePhoneNumber;
    private String peopleEtcMemo;
    private LocalDate dateCafe;
    private Double price;
}
