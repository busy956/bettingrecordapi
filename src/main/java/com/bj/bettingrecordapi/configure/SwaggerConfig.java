package com.bj.bettingrecordapi.configure;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import lombok.RequiredArgsConstructor;
import org.springdoc.core.models.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@OpenAPIDefinition(
        info = @Info(title = "내기기록  App",
                description = "내기기록으로 걸린 사람을 돈내게하자",
                version = "v1"))
@RequiredArgsConstructor
@Configuration

public class SwaggerConfig {
    @Bean
    public GroupedOpenApi v1OpenApi() {
        String[] paths = {"/v1/**"};

        return GroupedOpenApi.builder()
                .group("내기기록 API v1")
                .pathsToMatch(paths)
                .build();
    }
}
